package fiveelementsentertainment.sample;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.intrasonics.Decoder;

import fiveelementsentertainment.smarttones.STBrandEntry;
import fiveelementsentertainment.smarttones.STNetworkHelper;
import fiveelementsentertainment.smarttones.STPermissionsHelper;
import fiveelementsentertainment.smarttones.STToneManager;

public class MainActivity extends AppCompatActivity implements STToneManager.STBrandEntryDataSourceListener
{
    private static final String ST_CLIENT_ID = "0";
    // Token file name relative to the modules assets directory
    private static final String ST_INTRASONICS_TOKEN_FILE_NAME = "688/688.json";

    private TextView textView;

    @Override protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        this.textView = (TextView)this.findViewById(R.id.textView);

        if (STPermissionsHelper.confirmPermissions(this)) {
            this.onPermissionsGranted();
        }
    }

    @Override public void onRequestPermissionsResult(int requestCode,
                                                     @NonNull String[] permissions,
                                                     @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Toast toast;

        if (STPermissionsHelper.confirmPermissions(this)) {
            this.onPermissionsGranted();
            toast = Toast.makeText(this, "Permissions Granted", Toast.LENGTH_LONG);
        } else {
            toast = Toast.makeText(this, "Permissions Denied", Toast.LENGTH_LONG);
        }

        toast.show();
    }

    private void onPermissionsGranted()
    {
        STToneManager.sdkInitialize(this.getApplicationContext(), ST_CLIENT_ID, ST_INTRASONICS_TOKEN_FILE_NAME);
        STToneManager.i().addBrandEntryDataSourceListener(this);
    }

    @Override public void onToneManagerStatusUpdated()
    {
        this.textView.setText(this.statusTextForCurrentState());
    }

    @Override public void onBrandEntryDataSourceUpdated(STBrandEntry brandEntry)
    {
    }

    @Override public void onBrandEntryDataSourceChanged(STBrandEntry brandEntry)
    {
        this.textView.setText(String.format("Tone with toneID:[%d] recognized!", brandEntry.getToneID()));
    }

    private String statusTextForCurrentState()
    {
        String text = "";
        Decoder isDecoder = Decoder.getInstance();

        if (!STToneManager.i().isFinishedDownloadingToneData()) {
            if (!STToneManager.i().getIsFinishedDownloadingBrandEntries()) {
                text = "Downloading Tone Data";
            } else if (!STToneManager.i().getIsFinishedDownloadingBrandImages()) {
                int imageDownloadsTotal = STToneManager.i().getBrandImageDownloadsTotal();
                int imageDownloadsRemaining = STToneManager.i().getBrandImageDownloadsRemaining();
                int imageDownloadIndex = imageDownloadsTotal - imageDownloadsRemaining;
                text = String.format("Downloading Image %d of %d", imageDownloadIndex, imageDownloadsTotal);
            }
        } else {
            if (!STPermissionsHelper.hasRecordAudioPermission(this)) {
                text = "Missing Record Audio Permissions";
            } else {
                if (!STNetworkHelper.networkConnected(this)) {
                    text = "No Internet Connection";
                } else {
                    if (isDecoder != null) {
                        if (isDecoder.isListening()) {
                            text = "Listening For SmartTones";
                        } else if (isDecoder.isIdle()) {
                            text = "Decoder Idle";
                        } else {
                            text = "Decoder Not Initialized";
                        }
                    } else {
                        text = "Decoder Not Initialized";
                    }
                }
            }
        }

        return text;
    }
}




