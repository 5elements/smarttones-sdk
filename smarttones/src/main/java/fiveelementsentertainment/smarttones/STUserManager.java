package fiveelementsentertainment.smarttones;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by patsluth on 2016-10-11.
 */

public final class STUserManager extends STBaseManager
{
    private static STUserManager sharedInstance = null;

    private STUser user;

    /**
     * Shared Instance
     * @return
     */
    public static STUserManager i()
    {
        if (STUserManager.sharedInstance == null) {
            Log.e(STToneManager.class.getSimpleName(), "STUserManager singleton has not been initialized! Call initialize");
        }

        return STUserManager.sharedInstance;
    }

    protected static STUserManager initialize(Context context)
    {
        if (STUserManager.sharedInstance == null) {
            STUserManager.sharedInstance = new STUserManager(context);
        }

        return STUserManager.sharedInstance;
    }

    public STUserManager(Context base)
    {
        super(base);

        this.loadUserFromDisk();
    }

    public STUser getUser()
    {
        if (this.user == null) {
            this.user = new STUser();
        }
        return this.user;
    }

    public void setUser(STUser user)
    {
        this.user = user;
    }

    private void loadUserFromDisk()
    {
        System.out.println("loadUserFromDisk");

        if (this.getSharedPreferences().contains("user")) {
            try {
                STUser user = STUser.deserialize(this.getSharedPreferences().getString("user", ""));
                this.setUser(user);
            } catch (Exception e) { e.printStackTrace(); }
        }

        System.out.println("User: " + this.getUser());
    }

    public void saveUser()
    {
        System.out.println("saveUser");

        SharedPreferences.Editor editor = this.getSharedPreferences().edit();

        editor.remove("user");
        editor.putString("user", this.getUser().toJson());

        STToneManager.i().setToneDetectionEnabled(this.getUser().prefsToneDetectionEnabled);

        editor.apply();
    }

    public void logout(STUser user)
    {
        this.user = null;

        this.saveUser();
    }
}




