package fiveelementsentertainment.smarttones;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;
import com.intrasonics.heartbeatengine.BeatAtTimeParams;

import org.json.JSONArray;

import java.io.File;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

import static android.location.LocationManager.GPS_PROVIDER;

/**
 * Created by patsluth on 16-09-18.
 */
public final class STToneManager extends STBaseManager
{
    private static STToneManager sharedInstance = null;

    public static final String ST_API_URL = "http://codes.brandwoodglobal.com/service/"; //"http://ec2-54-164-239-202.compute-1.amazonaws.com/";
    public static final String ST_API_AUTH_TOKEN = "70c5142b-636a-49c9-b852-98b7e3b5c2e6";

    private HashMap<Long, STBrandEntry> allBrandEntries = new HashMap<>();
    private ArrayList<STBrandEntry> detectedBrandEntries = new ArrayList<>();
    private ArrayList<STBrandEntry> brandEntriesHome = new ArrayList<>();
    private ArrayList<STBrandEntry> brandEntriesFavorites = new ArrayList<>();
    private ArrayList<STBrandEntry> brandEntriesByBrand = new ArrayList<>();
    /**
     * All tones (detected or otherwise)
     * Key = toneID
     * Value = corresponding brand entry
     */
    public HashMap<Long, STBrandEntry> getAllBrandEntries() { return this.allBrandEntries; }
    public List<STBrandEntry> getDetectedBrandEntries() { return this.detectedBrandEntries; }
    public List<STBrandEntry> getBrandEntriesHome() { return this.brandEntriesHome; }
    public List<STBrandEntry> getBrandEntriesFavorites() { return this.brandEntriesFavorites; }
    public List<STBrandEntry> getBrandEntriesByBrand() { return this.brandEntriesByBrand; }

    private ArrayList<STBrandEntry> newBrandEntries = new ArrayList<>();
    public List<STBrandEntry> getNewBrandEntries() { return this.newBrandEntries; }

    private boolean isFinishedDownloadingBrandEntries = false;
    public boolean getIsFinishedDownloadingBrandEntries() { return this.isFinishedDownloadingBrandEntries; }
    private boolean isFinishedDownloadingBrandImages = false;
    public boolean getIsFinishedDownloadingBrandImages() { return this.isFinishedDownloadingBrandImages; }
    public boolean isFinishedDownloadingToneData() { return this.isFinishedDownloadingBrandEntries && this.isFinishedDownloadingBrandImages && !this.isDownloadingBrandImages(); }
    private CopyOnWriteArrayList<Pair<String, SimpleTarget<GlideDrawable>>> brandLandingImageRequests = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<Pair<String, SimpleTarget<GlideDrawable>>> brandLogoImageRequests = new CopyOnWriteArrayList<>();

    private STJSONRequestManager jsonRequestManager;
    public STJSONRequestManager getJsonRequestManager() { return this.jsonRequestManager; }

    private String clientID;
    private String intrasonicsTokenFileName;

    private Timer locationTimer;

    /**
     * Set Intrasonics token file name. Intrasonics service will be started if valid
     * @param intrasonicsTokenFileName
     */
    public void setIntrasonicsTokenFileName(String intrasonicsTokenFileName)
    {
        this.intrasonicsTokenFileName = intrasonicsTokenFileName;

        if (!TextUtils.isEmpty(this.intrasonicsTokenFileName)) {
            this.startIntrasonicsService();
        }
    }

    /*
     * Enable/Disable tone detection
     */
    private boolean toneDetectionEnabled = true;
    public void setToneDetectionEnabled(boolean toneDetectionEnabled)
    {
        this.toneDetectionEnabled = toneDetectionEnabled;
        this.requestStatusUpdate();
    }
    public boolean getToneDetectionEnabled() { return this.toneDetectionEnabled; }

    public interface STBrandEntryDataSourceListener
    {
        /*
        Invoked when the tone manager status changed. Update status related UI here
         */
        void onToneManagerStatusUpdated();

        /*
        Invoked when an existing brand entrys values are changed (ex isFavorited)
         */
        void onBrandEntryDataSourceUpdated(STBrandEntry brandEntry);
        /*
        Invoked when a brand entry is added
         */
        void onBrandEntryDataSourceChanged(STBrandEntry brandEntry);
    }

    private ArrayList<WeakReference<STBrandEntryDataSourceListener>> brandEntryDataSourceListeners = new ArrayList<>();
    public void addBrandEntryDataSourceListener(STBrandEntryDataSourceListener brandEntryDataSourceListener)
    {
        this.brandEntryDataSourceListeners.add(new WeakReference<STBrandEntryDataSourceListener>(brandEntryDataSourceListener));
    }

    private STToneManager(final Context context, @NonNull String clientID, @Nullable String intrasonicsTokenFileName)
    {
        super(context);

        this.jsonRequestManager = new STJSONRequestManager(context);
        this.clientID = clientID;
        this.setIntrasonicsTokenFileName(intrasonicsTokenFileName);

        STUserManager.initialize(context);    // initialize the user manager

        this.loadBrandEntriesFromDisk();
        this.downloadBrandEntriesFromServer();

        this.registerReceiver(this.onBrandEntryValueChangedBroadcastReceiver, STBrandEntry.onBrandEntryValueChangedIntentFilter);
        this.registerReceiver(this.onNetworkConnectivityChangeBroadcastReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        this.startLocationTimer(1000);
    }

    @Override protected void finalize() throws Throwable
    {
        this.unregisterReceiver(this.onBrandEntryValueChangedBroadcastReceiver);
        this.unregisterReceiver(this.onNetworkConnectivityChangeBroadcastReceiver);

        super.finalize();
    }

    /**
     * Shared Instance
     * @return
     */
    public static STToneManager i()
    {
        if (STToneManager.sharedInstance == null) {
            Log.e(STToneManager.class.getSimpleName(), "STToneManager singleton has not been initialized! Call sdkInitialize");
        }

        return STToneManager.sharedInstance;
    }

    /**
     * Initialize the SDK
     * @param context
     * @param clientID
     * @param intrasonicsTokenFileName
     * @return
     */
    public static STToneManager sdkInitialize(Context context, @NonNull String clientID, @Nullable String intrasonicsTokenFileName)
    {
        if (STToneManager.sharedInstance == null) {
            STToneManager.sharedInstance = new STToneManager(context, clientID, intrasonicsTokenFileName);
        }

        return STToneManager.sharedInstance;
    }

    // region Intrasonics

    private void startIntrasonicsService()
    {
        if (!TextUtils.isEmpty(this.intrasonicsTokenFileName)) {
            Intent intent = new Intent(this, STIntrasonicsService.class);
            intent.putExtra("Token", this.intrasonicsTokenFileName);
            this.startService(intent);

            this.requestStatusUpdate();
        }
    }

    // endregion

    private void loadBrandEntriesFromDisk()
    {
        if (this.getSharedPreferences().contains("globalBrandEntries")) {
            try {
                String jsonString = this.getSharedPreferences().getString("globalBrandEntries", "");
                JSONArray jsonArray = new JSONArray(jsonString);

                for (int i = 0; i < jsonArray.length(); i += 1) {
                    STBrandEntry brandEntry = STBrandEntry.deserialize(this, jsonArray.get(i).toString());
                    if (brandEntry != null) {
                        this.allBrandEntries.put(brandEntry.toneID, brandEntry);
                    }
                }
            } catch (Exception e) { e.printStackTrace(); }
        }

        if (this.getSharedPreferences().contains("brandEntries")) {
            try {
                String jsonString = this.getSharedPreferences().getString("brandEntries", "");
                JSONArray jsonArray = new JSONArray(jsonString);

                for (int i = 0; i < jsonArray.length(); i += 1) {
                    STBrandEntry brandEntry = STBrandEntry.deserialize(this, jsonArray.get(i).toString());

                    if (this.allBrandEntries.containsKey(brandEntry.toneID)) {
                        this.detectedBrandEntries.add(this.allBrandEntries.get(brandEntry.toneID));
                    }
                }
            } catch (Exception e) { e.printStackTrace(); }
        }

        this.updateBrandEntryArrays();
    }

    private void saveBrandEntries()
    {
       System.out.println("saveBrandEntries");

        SharedPreferences.Editor editor = this.getSharedPreferences().edit();

		String globalBrandEntriesJSONString = new Gson().toJson(this.allBrandEntries.values());
		editor.putString("globalBrandEntries", globalBrandEntriesJSONString);

		String brandEntriesJSONString = new Gson().toJson(this.detectedBrandEntries);
		editor.putString("brandEntries", brandEntriesJSONString);

        editor.apply();
    }

    private void updateBrandEntryArrays()
    {
        System.out.println("updateBrandEntryArrays");

        this.brandEntriesHome.clear();              // Recreate home array
        this.brandEntriesFavorites.clear();         // Recreate favorite array
        this.brandEntriesByBrand.clear();           // Recreate sorted by brand array

        this.newBrandEntries.clear();

		Iterator<STBrandEntry> iterator = this.detectedBrandEntries.iterator();
		while (iterator.hasNext()) {
			STBrandEntry brandEntry = iterator.next();
			if (brandEntry != null) {

				if (!brandEntry.getIsDeleted()) {
					System.out.println("Adding to home array: " + brandEntry.toneID);
					this.brandEntriesHome.add(brandEntry);
				}

				if (brandEntry.getIsFavorited() && !brandEntry.getIsDeleted())  {
					System.out.println("Adding to favorites array: " + brandEntry.toneID);
					this.brandEntriesFavorites.add(brandEntry);
				}

				if (brandEntry.getIsNew()) {
					this.newBrandEntries.add(brandEntry);
				}

			}
		}

        TreeSet<STBrandEntry> sortedBrandEntries = new TreeSet<STBrandEntry>(this.detectedBrandEntries);
        this.brandEntriesByBrand.addAll(sortedBrandEntries);
    }

    private void addOrUpdateGlobalBrandEntry(STBrandEntry brandEntry)
    {
        if (brandEntry != null) {

            if (this.allBrandEntries.containsKey(brandEntry.toneID)) {
                this.allBrandEntries.remove(brandEntry.toneID);
                System.out.println("Updating BrandEntry: " + brandEntry.toneID);

                try {

                    String url = brandEntry.getLandingPageZipURL();

                    if (url.contains("tone_landing_1479864267_35"))
                    STFileUtils.FileDownloadUtility.download(this,
                            url,
                            new STFileUtils.FileDownloadUtility.FileDownloadUtilityListener()
                            {
                                @Override public void onFileDownloadComplete(File file)
                                {
                                    STFileUtils.unzip(STToneManager.this,
                                            file,
                                            new STFileUtils.UnzipUtility.UnzipUtilityListener()
                                            {
                                                @Override public void onUnzipComplete(File rootDirectory) {
                                                    if (rootDirectory != null) {
                                                        System.out.println("onUnzipComplete " + rootDirectory.getAbsolutePath());

                                                        String htmlIndexFilePath = rootDirectory.getAbsolutePath() + File.separator + "index.html";
                                                        File htmlIndexFile = new File(htmlIndexFilePath);

                                                        System.out.println("index.html exists: " + htmlIndexFile.exists());
                                                    } else {
                                                        System.out.println("onUnzipComplete Error");
                                                    }
                                                }
                                            });
                                }
                            });
                } catch (Exception e) {
                }





            } else {
                System.out.println("Adding BrandEntry: " + brandEntry.toneID);
            }

            this.allBrandEntries.put(brandEntry.toneID, brandEntry);
        }
    }

    public void clearBrandEntries()
    {
        this.detectedBrandEntries.clear();
        this.updateBrandEntryArrays();
        this.saveBrandEntries();

        for (WeakReference<STBrandEntryDataSourceListener> weakReference : this.brandEntryDataSourceListeners) {
            if (weakReference.get() != null) {
                weakReference.get().onBrandEntryDataSourceChanged(null);
            }
        }
    }

    private void downloadBrandEntriesFromServer()
    {
        System.out.println("downloadBrandEntriesFromServer");

        if (this.isFinishedDownloadingBrandEntries) {
            this.downloadBrandImages();
            return;
        }

        String urlString = String.format("%sgetAllTones?authToken=%s&clientID=%s",
                STToneManager.ST_API_URL,
                STToneManager.ST_API_AUTH_TOKEN,
                this.clientID);

        // TODO: 2016-10-11 UNCOMMENT
//        if (BuildConfig.DEBUG) {
            urlString = String.format("%sgetAllTones?authToken=%s",
                    STToneManager.ST_API_URL,
                    STToneManager.ST_API_AUTH_TOKEN);
//        }

        this.jsonRequestManager.request(urlString, new STJSONRequestManager.STJSONRequestListener()
        {
            @Override public void onSuccess(TreeMap<String, Object> response)
            {
                try {

                    JSONArray toneListJSON = new JSONArray(response.get("ToneList").toString());

                    for (int i = 0; i < toneListJSON.length(); i += 1) {
                        STBrandEntry brandEntry = STBrandEntry.deserialize(STToneManager.this, toneListJSON.getString(i));
                        System.out.println(String.format("Loaded BrandEntry[%d] from server: %s", i, brandEntry.toneID));
                        STToneManager.this.addOrUpdateGlobalBrandEntry(brandEntry);
                    }

                    STToneManager.this.saveBrandEntries();
                    STToneManager.this.isFinishedDownloadingBrandEntries = true;
                    STToneManager.this.downloadBrandImages();

                } catch (Exception e) {
                    e.printStackTrace();
                    this.onError("Error Downloading Tones", null);
                }
            }

            @Override public void onError(String message, TreeMap<String, Object> jsonObject)
            {
                STToneManager.this.isFinishedDownloadingBrandEntries = false;
                // Try again in n seconds
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        STToneManager.this.downloadBrandEntriesFromServer();
                    }
                }, 1000);
            }
        });
    }

    public boolean isDownloadingBrandImages()
    {
        return (this.brandLandingImageRequests.size() > 0 || this.brandLogoImageRequests.size() > 0);
    }

    private void downloadBrandImages()
    {
       if (!STNetworkHelper.networkConnected(this) ||
               this.isDownloadingBrandImages() ||
               this.isFinishedDownloadingBrandImages) {
            return;
        }

        if (this.allBrandEntries.size() <= 0) {
            this.onDownloadBrandImagesComplete();
            return;
        }

        // Setup data structure to track download status
        for (final STBrandEntry brandEntry : this.allBrandEntries.values()) {
            if (brandEntry != null) {
                SimpleTarget<GlideDrawable> simpleTargetLanding = new SimpleTarget<GlideDrawable>() {
                    @Override public void onLoadFailed(Exception e, Drawable errorDrawable)
                    {
                        this.onGlideComplete();
                    }
                    @Override public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation)
                    {
                        this.onGlideComplete();
                    }
                    private void onGlideComplete()
                    {
                        STToneManager.this.onImageRequestComplete(this);
                    }
                };
                SimpleTarget<GlideDrawable> simpleTargetLogo = new SimpleTarget<GlideDrawable>() {
                    @Override public void onLoadFailed(Exception e, Drawable errorDrawable)
                    {
                        this.onGlideComplete();
                    }
                    @Override public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation)
                    {
                        this.onGlideComplete();
                    }
                    private void onGlideComplete()
                    {
                        STToneManager.this.onImageRequestComplete(this);
                    }
                };

                this.brandLandingImageRequests.add(new Pair<>(brandEntry.urlLandingImage, simpleTargetLanding));
                this.brandLogoImageRequests.add(new Pair<>(brandEntry.urlLogoImage, simpleTargetLogo));
            }
        }

        // FileDownload and crop landing images, only saving to disk
        for (Pair<String, SimpleTarget<GlideDrawable>> requestData : this.brandLandingImageRequests) {
            Glide.with(this)
                    .load(requestData.first)
                    .transform(new ImageCropTransformation(this))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .into(requestData.second);
        }
        // FileDownload logo images, only saving to disk
        for (Pair<String, SimpleTarget<GlideDrawable>> requestData : this.brandLogoImageRequests) {
            Glide.with(this)
                    .load(requestData.first)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .skipMemoryCache(true)
                    .into(requestData.second);
        }
    }

    private void onImageRequestComplete(SimpleTarget<GlideDrawable> imageRequest)
    {
        if (STToneManager.this.brandLogoImageRequests.size() > 0) {
            if (imageRequest != null) {
                int index = 0;
                for (int i = 0; i < this.brandLogoImageRequests.size(); i += 1) {
                    index = i;
                    Pair<String, SimpleTarget<GlideDrawable>> pair = this.brandLogoImageRequests.get(i);
                    if (pair.second == imageRequest) {
                        break;
                    }
                }
                this.brandLogoImageRequests.remove(index);
            }

            if (this.getBrandImageDownloadsRemaining() <= 0) {
                this.onDownloadBrandImagesComplete();
            } else {
                for (WeakReference<STBrandEntryDataSourceListener> weakReference : this.brandEntryDataSourceListeners) {
                    if (weakReference.get() != null) {
                        weakReference.get().onBrandEntryDataSourceUpdated(null);
                        weakReference.get().onToneManagerStatusUpdated();
                    }
                }
            }
        }
    }

    private void onDownloadBrandImagesComplete()
    {
        this.brandLandingImageRequests.clear();
        this.brandLogoImageRequests.clear();
        this.isFinishedDownloadingBrandImages = true;

        for (WeakReference<STBrandEntryDataSourceListener> weakReference : this.brandEntryDataSourceListeners) {
            if (weakReference.get() != null) {
                weakReference.get().onBrandEntryDataSourceUpdated(null);
                weakReference.get().onToneManagerStatusUpdated();
            }
        }
    }

    public int getBrandImageDownloadsTotal()
    {
        int imageDownloadsTotal = this.allBrandEntries.size() * 2;  // Landing and Logo
        return imageDownloadsTotal;
    }

    public int getBrandImageDownloadsRemaining()
    {
        int imageDownloadsRemaining = this.brandLogoImageRequests.size();
        return imageDownloadsRemaining;
    }

    private void cancelAllImageRequests()
    {
        for (Pair<String, SimpleTarget<GlideDrawable>> requestData : STToneManager.this.brandLandingImageRequests) {
            Glide.clear(requestData.second);
        }
        for (Pair<String, SimpleTarget<GlideDrawable>> requestData : STToneManager.this.brandLogoImageRequests) {
            Glide.clear(requestData.second);
        }
        STToneManager.this.brandLandingImageRequests.clear();
        STToneManager.this.brandLogoImageRequests.clear();

        for (WeakReference<STBrandEntryDataSourceListener> weakReference : this.brandEntryDataSourceListeners) {
            if (weakReference.get() != null) {
                weakReference.get().onBrandEntryDataSourceUpdated(null);
                weakReference.get().onToneManagerStatusUpdated();
            }
        }
    }

    /**
     * Test method to simulate a detected tone
     * @param toneID
     */
    public void simulateToneDetected(long toneID)
    {
        if (!BuildConfig.DEBUG) {
            this.onToneDetected(toneID);
        }
    }

    protected void onToneDetected(long toneID)
    {
        if (!this.toneDetectionEnabled) {
            return;
        }

        System.out.println("onToneDetected: " +  toneID);

		Iterator<STBrandEntry> iterator = this.detectedBrandEntries.iterator();
		while (iterator.hasNext()) {
			STBrandEntry brandEntry = iterator.next();
			if (brandEntry != null && brandEntry.toneID == toneID) {
				System.out.println(String.format("Tone %d, has already been detected", toneID));
				brandEntry.setIsDeleted(false, false);
				iterator.remove();
				break;
			}
		}

        if (!this.allBrandEntries.containsKey(toneID)) {
            System.out.println(String.format("Unknown tone %d", toneID));
            return;
        }

        try {

            // Find detected tone and add it to the recievedBrandEntry array
			STBrandEntry brandEntry = this.allBrandEntries.get(toneID);

            if (brandEntry != null) {

				brandEntry.setIsNew(true, false);
//				brandEntry.setIsDeleted(false, false);
//				brandEntry.setIsFavorited(false, false);

                this.detectedBrandEntries.add(0, brandEntry);

				this.saveBrandEntries();
				this.updateBrandEntryArrays();

				if (brandEntry != null) {
					for (WeakReference<STBrandEntryDataSourceListener> weakReference : this.brandEntryDataSourceListeners) {
						if (weakReference.get() != null) {
							weakReference.get().onBrandEntryDataSourceChanged(brandEntry);
						}
					}
				}
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onHeartbeatDetected(BeatAtTimeParams beatAtTimeParams)
    {
        Map<String, String> eventParams = STAnalytics.getEventParamsForBrandEntry(null);

        eventParams.put("ChannelNumber", Integer.toString(beatAtTimeParams.mChannelNumber));

        STAnalytics.logEvent(this, "Heartbeat_Received", eventParams);
    }

    private BroadcastReceiver onBrandEntryValueChangedBroadcastReceiver = new BroadcastReceiver()
    {
        @Override public void onReceive(Context context, Intent intent)
        {
			if (intent != null &&
					intent.hasExtra("toneID") &&
					intent.hasExtra("isDeletedValueChanged") &&
					intent.hasExtra("isFavoritedValueChanged") &&
					intent.hasExtra("isNewValueChanged")) {

				long toneID = intent.getLongExtra("toneID", 0);
				boolean isDeletedValueChanged = intent.getBooleanExtra("isDeletedValueChanged", false);
				boolean isFavoritedValueChanged = intent.getBooleanExtra("isFavoritedValueChanged", false);
				boolean isNewValueChanged = intent.getBooleanExtra("isNewValueChanged", false);

				STBrandEntry brandEntry = STToneManager.i().allBrandEntries.get(toneID);

				if (brandEntry == null) {
					return;
				}

				// Update data source
				STToneManager.this.saveBrandEntries();
				STToneManager.this.updateBrandEntryArrays();

				// Notify listeners of update
				for (WeakReference<STBrandEntryDataSourceListener> weakReference : STToneManager.this.brandEntryDataSourceListeners) {
					if (weakReference.get() != null) {
						weakReference.get().onBrandEntryDataSourceUpdated(brandEntry);
					}
				}

				// Analytics
				Map<String, String> eventParams = STAnalytics.getEventParamsForBrandEntry(brandEntry);
				if (isDeletedValueChanged && brandEntry.getIsDeleted()) {
					STAnalytics.logEvent(STToneManager.this, STAnalytics.ANALYTICS_EVENT_TYPE_TONE_DELETED, eventParams);
				} else if (isFavoritedValueChanged) {
					String eventType = (brandEntry.getIsFavorited()) ? STAnalytics.ANALYTICS_EVENT_TYPE_TONE_FAVORITED : STAnalytics.ANALYTICS_EVENT_TYPE_TONE_UNFAVORITED;
					STAnalytics.logEvent(STToneManager.this, eventType, eventParams);
				} else  if (isNewValueChanged) {
					// dont log
				}
			}
        }
    };

    /**
     * Request a status update for all STBrandEntryDataSourceListener's
     */
    public void requestStatusUpdate()
    {
        for (WeakReference<STBrandEntryDataSourceListener> weakReference : this.brandEntryDataSourceListeners) {
            if (weakReference.get() != null) {
                weakReference.get().onToneManagerStatusUpdated();
            }
        }
    }

    private BroadcastReceiver onNetworkConnectivityChangeBroadcastReceiver = new BroadcastReceiver()
    {
        @Override public void onReceive(Context context, Intent intent)
        {
            if (!STNetworkHelper.networkConnected(STToneManager.this)) {
                STToneManager.this.cancelAllImageRequests();
            } else {
                STToneManager.this.downloadBrandEntriesFromServer();
            }

            for (WeakReference<STBrandEntryDataSourceListener> weakReference : STToneManager.this.brandEntryDataSourceListeners) {
                if (weakReference.get() != null) {
                    weakReference.get().onToneManagerStatusUpdated();
                }
            }
        }
    };

    public String urlEncode(String source)
    {
        try {
            return URLEncoder.encode(source, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return source;
        }
    }

    private void startLocationTimer(long milliseconds)
    {
        if (this.locationTimer != null) {
            return;
        }

        this.locationTimer = new Timer();
        this.locationTimer.schedule(new TimerTask()
        {
            @Override public void run()
            {
                STToneManager.this.locationTimer = null;
                STToneManager.this.startLocationTimer(1200000); // 1200000 ms = 20 min

                if (STPermissionsHelper.hasLocationPermission(STToneManager.this)) {
                    try {
                        String locationProvider = GPS_PROVIDER;
//                    locationProvider = NETWORK_PROVIDER;
                        LocationManager locationManager = (LocationManager)STToneManager.this.getSystemService(Context.LOCATION_SERVICE);
                        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

                        Map<String, String> eventParams = STAnalytics.getEventParamsForBrandEntry(null);
                        eventParams.put("Latitude", Double.toString(lastKnownLocation.getLatitude()));
                        eventParams.put("Longitude", Double.toString(lastKnownLocation.getLongitude()));
                        STAnalytics.logEvent(STToneManager.this, "User_Location", eventParams);

                    } catch (SecurityException e) { }
                }
            }
        }, milliseconds);
    }





    public static class ImageCropTransformation extends BitmapTransformation
    {
        public ImageCropTransformation(Context context)
        {
            super(context);
        }

        @Override protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight)
        {
            float ratio = (float)outHeight / (float)outWidth;
            float targetWidth = (float)toTransform.getWidth();
            float targetHeight = targetWidth * ratio;

            return Bitmap.createBitmap(toTransform, 0, 0, Math.round(targetWidth), Math.round(targetHeight));
        }

        @Override public String getId()
        {
            return this.getClass().getSimpleName();
        }
    }





}




