package fiveelementsentertainment.smarttones;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.intrasonics.Decoder;
import com.intrasonics.DecoderConfidence;
import com.intrasonics.DecoderListener;
import com.intrasonics.heartbeatengine.BeatAtTimeParams;
import com.intrasonics.heartbeatengine.HeartbeatError;
import com.intrasonics.heartbeatengine.IHeartbeatEngine;
import com.intrasonics.heartbeatengine.IHeartbeatEngineListener;
import com.intrasonics.triggerengine.Codeword;
import com.intrasonics.triggerengine.ITriggerEngine;
import com.intrasonics.triggerengine.ITriggerEngineListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;

/**
 * Created by patsluth on 16-08-22.
 */
public class STIntrasonicsService extends IntentService implements DecoderListener, ITriggerEngineListener, IHeartbeatEngineListener
{
    public STIntrasonicsService()
    {
        super(STIntrasonicsService.class.getSimpleName());
    }

    public STIntrasonicsService(String name)
    {
        super(name);
    }

    @Override protected void onHandleIntent(Intent intent)
    {

    }

    // region Intrasonics

    private IBinder iBinder;
    private Decoder isDecoder = null;

    // endregion

    @Override public void onCreate()
    {
        super.onCreate();
    }

    @Override public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent != null && intent.hasExtra("Token")) {
            String tokenFileName = intent.getStringExtra("Token");
            this.initializeIntrasonicsDecoder(tokenFileName);
            this.isDecoder.startDecoding();
        }

        return START_STICKY;
    }

    @Override public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override public void onRebind(Intent intent)
    {
        super.onRebind(intent);
    }

    @Override public boolean onUnbind(Intent intent)
    {
        return super.onUnbind(intent);
    }

    // region Intrasonics

    private void initializeIntrasonicsDecoder(String tokenFileName)
    {
        if (this.isDecoder != null) {
            return;
        }

        this.isDecoder = Decoder.getInstance();
        this.isDecoder.setApplicationContext(this.getApplicationContext());
        this.isDecoder.registerListener(this);

        String pluginPath = this.isDecoder.getFilePathForPlugin("intrasonics_TestPlugin");
        String token = this.readStringFromFile(tokenFileName);                              // Read in licence token

        if (token.length() <= 0) {
            this.intrasonicsUpdateHandler.sendMessage(this.intrasonicsUpdateHandler.obtainMessage(ISMessageType.FAILED.getValue(), "Failed to read Token"));
        } else {
            this.isDecoder.setToken(token);                                                // Pass licence token to decoder

            this.intrasonicsUpdateHandler.sendMessage(this.intrasonicsUpdateHandler.obtainMessage(ISMessageType.MESSAGE.getValue(), "Plugin Path: " + pluginPath));
            this.isDecoder.connectEngineListener(this, ITriggerEngine.class);
            this.isDecoder.connectEngineListener(this, IHeartbeatEngine.class);
        }
    }

    /**
     * Handler on the main thread, used when an Intrasonics Decoder event
     * or error is received, to transfer processing onto the main thread.
     */
    private Handler intrasonicsUpdateHandler = new Handler()
    {
        public void handleMessage(Message message)
        {
            ISMessageType messageType = ISMessageType.forValue(message.what);

            switch (messageType)
            {
                case MESSAGE: {
//                    Log.d(getClass().getSimpleName(), "Message: " + message.obj.toString());
                    try {
                        STToneManager.i().requestStatusUpdate(); } catch (Exception e) {}
                }
                break;
                case DATARECEIVED: {
                    if (message.obj instanceof Codeword) {
                        Codeword codeword = (Codeword)message.obj;
                        if (STToneManager.i() != null) {
                            STToneManager.i().onToneDetected(codeword.codeword);
                        }
                    } else if (message.obj instanceof BeatAtTimeParams) {   // Heartbeat
                        BeatAtTimeParams beatAtTimeParams = (BeatAtTimeParams)message.obj;
                        if (STToneManager.i() != null) {
                            STToneManager.i().onHeartbeatDetected(beatAtTimeParams);
                        }
                    }
                }
                break;
                case CONFIDENCE: {
                    if (message.obj instanceof DecoderConfidence) {
                        DecoderConfidence decoderConfidence = (DecoderConfidence)message.obj;
//                        Log.d(getClass().getSimpleName(), "DecoderConfidence: " + decoderConfidence.toString());
                    }
                }
                break;
                case FAILED: {
//                    Log.d(getClass().getSimpleName(), "Intrasonics Error: " + message.obj.toString());
                    if (STToneManager.i() != null) {
                        STToneManager.i().requestStatusUpdate();
                    }
                }
                break;
                case STARTEDLISTENING: {
                    if (STToneManager.i() != null) {
                        STToneManager.i().requestStatusUpdate();
                    }
                }
                    break;
            }
        }
    };

    public void onDecoderFailed(String error, Date time)
    {
        this.intrasonicsUpdateHandler.sendMessage(this.intrasonicsUpdateHandler.obtainMessage(ISMessageType.MESSAGE.getValue(), error));
    }

    public void onDecoderConfidenceChanged(DecoderConfidence confidence)
    {
        this.intrasonicsUpdateHandler.sendMessage(this.intrasonicsUpdateHandler.obtainMessage(ISMessageType.CONFIDENCE.getValue(), confidence));
    }

    public void onDecoderStartedListening()
    {
        this.intrasonicsUpdateHandler.sendMessage(this.intrasonicsUpdateHandler.obtainMessage(ISMessageType.STARTEDLISTENING.getValue(), "Decoder has started listening"));
    }

    public void onDecoderResetComplete()
    {
        this.intrasonicsUpdateHandler.sendMessage(this.intrasonicsUpdateHandler.obtainMessage(ISMessageType.MESSAGE.getValue(), "Decoder has been reset"));
    }

    public void onDecoderIdle()
    {
        this.intrasonicsUpdateHandler.sendMessage(this.intrasonicsUpdateHandler.obtainMessage(ISMessageType.MESSAGE.getValue(), "Decoder is idle"));
    }

    /**
     * ITriggerEngine
     */
    @Override public void onCodewordReceived(Codeword var1)
    {
        this.intrasonicsUpdateHandler.sendMessage(this.intrasonicsUpdateHandler.obtainMessage(ISMessageType.DATARECEIVED.getValue(), var1));
    }

    /**
     * IHeartbeatEngine
     */
    public void onHBEngineDidBeatAtTime(BeatAtTimeParams beatAtTimeParams)
    {
        this.intrasonicsUpdateHandler.sendMessage(this.intrasonicsUpdateHandler.obtainMessage(ISMessageType.DATARECEIVED.getValue(), beatAtTimeParams));
    }

    @Override public void onHBEngineDidFailWithError(HeartbeatError heartbeatError)
    {
        this.intrasonicsUpdateHandler.sendMessage(this.intrasonicsUpdateHandler.obtainMessage(ISMessageType.FAILED.getValue(), heartbeatError));
    }

    public enum ISMessageType
    {
        NONE(-1),
        MESSAGE(0),
        DATARECEIVED(1),
        CONFIDENCE(2),
        FAILED(3),
        STARTEDLISTENING(4),
        ;

        private int value;

        ISMessageType(int value)
        {
            this.value = value;
        }

        public int getValue()
        {
            return value;
        }

        public static ISMessageType forValue(int value)
        {
            switch (value) {
                case 0:
                    return ISMessageType.MESSAGE;
                case 1:
                    return ISMessageType.DATARECEIVED;
                case 2:
                    return ISMessageType.CONFIDENCE;
                case 3:
                    return ISMessageType.FAILED;
                case 4:
                    return ISMessageType.STARTEDLISTENING;
                default:
                    return ISMessageType.NONE;
            }
        }
    }

    /**
     * Read a token file into a string, ready for passing to the decoder.
     *
     * @param fileName The name of the file containing the token data
     *
     * @return A String holding the contents of the specified file
     *
     * @throws IOException Upon failure to open and read the file
     */
    public String readStringFromFile(String fileName)
    {
        String string = "";

        try {

            InputStream iS = this.getApplicationContext().getResources().getAssets().open(fileName);

            byte[] buffer = new byte[iS.available()];
            iS.read(buffer);

            ByteArrayOutputStream oS = new ByteArrayOutputStream();

            oS.write(buffer);
            string = oS.toString();
            oS.close();
            iS.close();

        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "Failed to open file: " + e.getMessage());
        }

        return string;
    }
}




