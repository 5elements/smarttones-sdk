package fiveelementsentertainment.smarttones;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by patsluth on 2016-10-11.
 */

public class STUser
{
    public String email = "";
    public ServiceType loginType = ServiceType.NONE;
    public String authToken = "";
    public String userID = "";
    public String age = "";
    public String gender = "";
    public String name = "";
    public String serviceUserIdentifier = "";

    public boolean prefsToneDetectionEnabled = true;
    public boolean prefsTextEnabled = true;
    public boolean prefsEmailEnabled = true;

    public STUser()
    {
        super();
    }

    public static STUser deserialize(String json)
    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(STUser.class, new Deserializer());
        Gson gson = gsonBuilder.create();
        STUser user = gson.fromJson(json, STUser.class);

        return user;
    }

    public boolean isLoggedIn()
    {
        return (this.loginType != ServiceType.NONE);// && this.getUser().authToken.length() > 0);
    }

    public String toJson()
    {
        return new Gson().toJson(this);
    }

    public String toString()
    {
        return this.toJson();
    }




    private static class Deserializer implements JsonDeserializer<STUser>
    {
        @Override public STUser deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
        {
            JsonObject jsonObject = (JsonObject)json;
            STUser user = new STUser();

            if (jsonObject.has("email")) { user.email = jsonObject.get("email").getAsString(); }
            if (jsonObject.has("loginType")) { user.loginType = ServiceType.getServiceTypeForString(jsonObject.get("loginType").getAsString()); }
            if (jsonObject.has("authToken")) { user.authToken = jsonObject.get("authToken").getAsString(); }
            if (jsonObject.has("userID")) { user.userID = jsonObject.get("userID").getAsString(); }
            if (jsonObject.has("age")) { user.age = jsonObject.get("age").getAsString(); }
            if (jsonObject.has("gender")) { user.gender = jsonObject.get("gender").getAsString(); }
            if (jsonObject.has("name")) { user.name = jsonObject.get("name").getAsString(); }
            if (jsonObject.has("serviceUserIdentifier")) { user.serviceUserIdentifier = jsonObject.get("serviceUserIdentifier").getAsString(); }

            if (jsonObject.has("prefsToneDetectionEnabled")) { user.prefsToneDetectionEnabled = jsonObject.get("prefsToneDetectionEnabled").getAsBoolean(); }
            if (jsonObject.has("prefsTextEnabled")) { user.prefsTextEnabled = jsonObject.get("prefsTextEnabled").getAsBoolean(); }
            if (jsonObject.has("prefsEmailEnabled")) { user.prefsEmailEnabled = jsonObject.get("prefsEmailEnabled").getAsBoolean(); }

            return user;
        }
    }



    public enum ServiceType
    {
        NONE(-1),
        EMAIL(0),
        FACEBOOK(1),
        TWITTER(2),
        ;

        private static final String LOGINTYPE_NONE = "none";
        private static final String LOGINTYPE_EMAIL = "email";
        private static final String LOGINTYPE_FACEBOOK = "facebook";
        private static final String LOGINTYPE_TWITTER = "twitter";

        private int serviceCode;

        private ServiceType(int serviceCode)
        {
            this.serviceCode = serviceCode;
        }

        public static ServiceType getServiceTypeForString(String serviceType)
        {
            switch (serviceType) {
                case LOGINTYPE_EMAIL:
                    return NONE;
                case LOGINTYPE_FACEBOOK:
                    return FACEBOOK;
                case LOGINTYPE_TWITTER:
                    return TWITTER;
                default:
                    return NONE;
            }
        }

        public String getServiceName()
        {
            switch (this.serviceCode) {
                case 0:
                    return LOGINTYPE_EMAIL;
                case 1:
                    return LOGINTYPE_FACEBOOK;
                case 2:
                    return LOGINTYPE_TWITTER;
                default:
                    return LOGINTYPE_NONE;
            }
        }

        public int getServiceCode()
        {
            return this.serviceCode;
        }

        public String toString()
        {
            return this.getServiceName();
        }
    }
}




