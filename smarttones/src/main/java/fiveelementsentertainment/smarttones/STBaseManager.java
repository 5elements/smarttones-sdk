package fiveelementsentertainment.smarttones;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;

/**
 * Created by patsluth on 16-09-18.
 */
public abstract class STBaseManager extends ContextWrapper
{
    private SharedPreferences sharedPreferences;

    public STBaseManager(Context context)
    {
        super(context);

        String presfId = String.format("%s%s", this.getPackageName(), "_Preferences");
        sharedPreferences = this.getSharedPreferences(presfId, Context.MODE_PRIVATE);
    }

    public SharedPreferences getSharedPreferences()
    {
        String presfId = String.format("%s%s", this.getPackageName(), "_Preferences");
        return this.getSharedPreferences(presfId, Context.MODE_PRIVATE);
    }
}




