package fiveelementsentertainment.smarttones;

import android.content.Context;
import android.content.ContextWrapper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.TreeMap;

/**
 * Created by patsluth on 16-08-10.
 */
public final class STJSONRequestManager extends ContextWrapper
{
    private static final String ST_JSONRESULTCODE_SUCCESS = "success";
    private static final String ST_JSONRESULTCODE_ERROR = "error";

    private RequestQueue requestQueue;
    public RequestQueue getRequestQueue() { return this.requestQueue; }

    public STJSONRequestManager(Context context)
    {
        super(context);

        this.requestQueue = Volley.newRequestQueue(context);
    }

    public interface STJSONRequestListener
    {
        public void onSuccess(TreeMap<String, Object> response);
        public void onError(@Nullable String message, @Nullable TreeMap<String, Object> response);
    }

    public void request(final String urlString, @NonNull final STJSONRequestListener onJsonObjectRequestListener)
    {
        if (!STNetworkHelper.networkConnected(this)) {
            onJsonObjectRequestListener.onError("No Internet Connection", null);
            return;
        }

        System.out.println(urlString);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlString,
                new Response.Listener<JSONObject>() {
                    @Override public void onResponse(JSONObject response)
                    {
                        TreeMap<String, Object> responseMap = new TreeMap<String, Object>();
                        Iterator<String> keyItr = response.keys();

                        while (keyItr.hasNext()) {
                            String key = keyItr.next();
                            try {
                                responseMap.put(key, response.get(key));
                            } catch (Exception e) {  }
                        }

                        try {
                            String status = responseMap.get("status").toString();

                            if (status.equals(ST_JSONRESULTCODE_SUCCESS)) {
                                onJsonObjectRequestListener.onSuccess(responseMap);
                            } else if (status.equals(ST_JSONRESULTCODE_ERROR)) {
                                String message = response.getString("msg");
                                onJsonObjectRequestListener.onError(message, responseMap);
                            }
                        } catch (Exception e) {
                            onJsonObjectRequestListener.onError(null, null);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override public void onErrorResponse(VolleyError error)
                    {
                        onJsonObjectRequestListener.onError(null, null);
                    }
        });

        this.requestQueue.add(jsonObjectRequest);
    }
}




