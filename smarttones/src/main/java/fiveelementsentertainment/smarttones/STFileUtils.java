package fiveelementsentertainment.smarttones;

import android.content.Context;
import android.icu.util.Output;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;

import com.google.common.io.Files;
import com.sromku.simple.storage.ExternalStorage;
import com.sromku.simple.storage.SimpleStorage;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Created by patsluth on 16-10-02.
 */
public class STFileUtils
{
    public static final String DOCUMENTS_DIRECTORY_NAME = "Documents";

    public static final ExternalStorage externalStorage()
    {
        return SimpleStorage.getExternalStorage();
    }

    /**
     * Extract a zip file to it's current directory
     * @param context
     * @param file
     * @return root directory of unzipped file
     */
    public static void unzip(Context context, File file, UnzipUtility.UnzipUtilityListener unzipUtilityListener)
    {
        UnzipUtility unzipUtility = new UnzipUtility();
        try {
            StringBuilder unzipFilePath = new StringBuilder(file.getParentFile().getAbsolutePath());
            unzipUtility.unzip(file, unzipFilePath.toString(), unzipUtilityListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get md5 hash for file
     * @param file
     * @return
     */
    public static String MD5(File file)
    {
        String md5 = "";

        if (file.exists()) {
            try {
                MessageDigest messageDigest = MessageDigest.getInstance("MD5");
                InputStream inputStream = new FileInputStream(file);
                DigestInputStream digestInputStream = new DigestInputStream(inputStream, messageDigest);
                md5 = Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT);
            } catch (Exception e) { }
        }

        return md5;
    }





    public static class FileDownloadUtility
    {
        /**
         * Downloads a file at url to the Documents Directory
         * @param context
         * @param url
         * @param fileDownloadUtilityListener
         */
        public static void download(final Context context, String url, final FileDownloadUtilityListener fileDownloadUtilityListener)
        {
            System.out.printf("download: %s", url);

            String fileName = Files.getNameWithoutExtension(url) + "." + Files.getFileExtension(url);
            Uri uri = Uri.parse(url);
            Uri destinationUri = Uri.parse(context.getExternalCacheDir().toString() + File.separator + fileName);
            File file = externalStorage().getFile(DOCUMENTS_DIRECTORY_NAME, fileName);

            if (file.exists()) {

                System.out.printf("File exists, skipping download");

                String md5 = STFileUtils.MD5(file);

                if (!TextUtils.isEmpty(md5)) {

                }

                if (fileDownloadUtilityListener != null) {
                    fileDownloadUtilityListener.onFileDownloadComplete(file);
                }

            } else {

                DownloadRequest downloadRequest = new DownloadRequest(uri)
                        .setDestinationURI(destinationUri)
                        .setDownloadContext(context)
                        .setStatusListener(new DownloadStatusListenerV1()
                        {
                            @Override public void onDownloadComplete(DownloadRequest downloadRequest)
                            {
                                System.out.println("onDownloadComplete: " + downloadRequest.getUri());

                                File file = new File(downloadRequest.getDestinationURI().getPath());

                                if (fileDownloadUtilityListener != null) {
                                    fileDownloadUtilityListener.onFileDownloadComplete(file);
                                }
                            }

                            @Override public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage)
                            {
                                System.out.println("onDownloadFailed: " + downloadRequest.getUri());

                                if (fileDownloadUtilityListener != null) {
                                    fileDownloadUtilityListener.onFileDownloadComplete(null);
                                }
                            }

                            @Override public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress)
                            {
                            }
                        });
                ThinDownloadManager thinDownloadManager = new ThinDownloadManager();
                thinDownloadManager.add(downloadRequest);
            }
        }




        public static interface FileDownloadUtilityListener
        {
            /**
             * File download callback. File will be null if download failed
             * @param file
             */
            public void onFileDownloadComplete(@Nullable File file);
        }
    }





    public static class UnzipUtility
    {
        private static final int BUFFER_SIZE = 4096;

        /**
         * Extracts a zip file specified by the zipFilePath to a directory specified by
         * destDirectory (will be created if does not exists)
         *
         * @param zipFile
         * @param unzipFilePath
         * @param unzipUtilityListener
         */
        public static void unzip(File zipFile, String unzipFilePath, UnzipUtilityListener unzipUtilityListener)
        {
            System.out.printf("unzip: zipFilePath: %s\n", zipFile.getAbsolutePath());
            System.out.printf("unzip: destinationDirectory: %s\n", unzipFilePath);

            String path = unzipFilePath + File.separator;

            InputStream inputStream;
            ZipInputStream zipInputStream;

            try {
                String filename;
                inputStream = new FileInputStream(path + zipFile.getName());
                zipInputStream = new ZipInputStream(new BufferedInputStream(inputStream));
                ZipEntry zipEntry = null;
                String zipRootDirectory = null;
                byte[] buffer = new byte[1024];
                int count;

                while ((zipEntry = zipInputStream.getNextEntry()) != null) {

                    if (zipRootDirectory == null){
                        zipRootDirectory = zipEntry.getName().split("/")[0];
                    }

                    filename = zipEntry.getName();

                    // Need to create directories if not exists, or
                    // it will generate an Exception...
                    if (zipEntry.isDirectory()) {
                        File fmd = new File(path + filename);
                        fmd.mkdirs();
                        continue;
                    }

                    FileOutputStream fout = new FileOutputStream(path + filename);

                    while ((count = zipInputStream.read(buffer)) != -1) {
                        fout.write(buffer, 0, count);
                    }

                    fout.close();
                    zipInputStream.closeEntry();
                }

                zipInputStream.close();




                // Rename the root of the unzipped file to match the downloaded zip filename
                File zipRootDirectoryFile = new File(path + zipRootDirectory);
                if (zipRootDirectoryFile.exists()) {
                    File renameFile = new File(path + File.separator + Files.getNameWithoutExtension(zipFile.getName()) + File.separator);
                    System.out.println("Renaming " + zipRootDirectory + " to " + renameFile);
                    zipRootDirectoryFile.renameTo(renameFile);

                    if (unzipUtilityListener != null) {
                        unzipUtilityListener.onUnzipComplete(zipRootDirectoryFile);
                        return;
                    }
                } else {
                    System.out.println("Error: zipRootDirectory does not exist.");
                }

            } catch(IOException e) {
                e.printStackTrace();
            }

            if (unzipUtilityListener != null) {
                unzipUtilityListener.onUnzipComplete(null);
            }
        }

        /**
         * Extracts a zip entry (file entry)
         *
         * @param zipIn
         * @param filePath
         * @throws IOException
         */
        private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException
        {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
            byte[] bytesIn = new byte[BUFFER_SIZE];
            int read = 0;
            while ((read = zipIn.read(bytesIn)) != -1) {
                bos.write(bytesIn, 0, read);
            }
            bos.close();
        }





        public static interface UnzipUtilityListener
        {
            /**
             * Unzip callback. rootDirectory will be null if unzip failed
             *
             * @param rootDirectory
             */
            public void onUnzipComplete(@Nullable File rootDirectory);
        }
    }





}




