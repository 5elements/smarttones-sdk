package fiveelementsentertainment.smarttones;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.util.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by patsluth on 16-07-28.
 */
public class STBrandEntry implements Serializable, Comparable<STBrandEntry>
{
    private transient Context context;

    @SerializedName("Tone_ID") protected Long toneID = new Long(0);
    @SerializedName("clientID") protected String clientID = "";
    @SerializedName("Ad_ID") protected Integer adID = 0;
    @SerializedName("Channel_ID") protected Integer channelID = 0;
    @SerializedName("Like_Count") protected Integer likeCount = 0;

    @SerializedName("Brand_Name") protected String brandName = "";
    @SerializedName("Media_Name") protected String mediaName = "";
    @SerializedName("URL_LogoImage") protected String urlLogoImage = "";
    @SerializedName("URL_LandingImage") protected String urlLandingImage = "";
    @SerializedName("Promo_Title") protected String promoTitle = "";
    @SerializedName("Promo_Text") protected String promoText = "";
    @SerializedName("landingPageZip") protected String landingPageZipURL = "";

    @SerializedName("Button_Text") private Map<String, String> buttonNames = new TreeMap<String, String>();
    @SerializedName("URL_ClickThru") private Map<String, String> buttonURLs = new TreeMap<String, String>();
    @SerializedName("Button_Color_FG") private Map<String, String> buttonTextColors = new TreeMap<String, String>();
    @SerializedName("Button_Color_BG") private Map<String, String> buttonBackgroundColors = new TreeMap<String, String>();
    @SerializedName("actionType") private Map<String, String> buttonActionTypes = new TreeMap<String, String>();
    @SerializedName("eventTitle") private Map<String, String> buttonEventTitles = new TreeMap<String, String>();
    @SerializedName("eventNotes") private Map<String, String> buttonEventNotes = new TreeMap<String, String>();
    @SerializedName("locationName") private Map<String, String> buttonLocationNames = new TreeMap<String, String>();
    @SerializedName("locationLat") private Map<String, String> buttonLocationLats = new TreeMap<String, String>();
    @SerializedName("locationLong") private Map<String, String> buttonLocationLongs = new TreeMap<String, String>();
    @SerializedName("dateTime") private Map<String, String> buttonDateTimes = new TreeMap<String, String>();
    @SerializedName("emailToAddress") private Map<String, String> buttonEmailToAddresses = new TreeMap<String, String>();
    @SerializedName("emailSubject") private Map<String, String> buttonEmailSubjects = new TreeMap<String, String>();
    @SerializedName("emailBody") private Map<String, String> buttonEmailBodys = new TreeMap<String, String>();
    @SerializedName("phoneNumber") private Map<String, String> buttonPhoneNumbers = new TreeMap<String, String>();
    @SerializedName("fbURL") private Map<String, String> buttonFBURLs = new TreeMap<String, String>();
    @SerializedName("textMessageBody") private Map<String, String> buttonTextMessageBodys = new TreeMap<String, String>();

    private Boolean isDeleted = false;
    private Boolean isFavorited = false;
    private Boolean isNew = false;

    private transient ArrayList<ButtonData> buttonDataList = new ArrayList<ButtonData>();

    /**
     * Intent Filter for BroadcastReceiver. Contains Extras
     *
     *      long toneID
     *      boolean isDeletedValueChanged
     *      boobooleanl isFavoritedValueChanged
     *      boolean isNewValueChanged
     */
    public static transient final IntentFilter onBrandEntryValueChangedIntentFilter = new IntentFilter("onBrandEntryIsNewStateChanged");

    public STBrandEntry(Context context)
    {
        super();

        this.context = context;
    }

    public static STBrandEntry deserialize(Context context, String json)
    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(STBrandEntry.class, new Deserializer(context));
        Gson gson = gsonBuilder.create();

        STBrandEntry brandEntry = null;

        try {
            brandEntry = gson.fromJson(json, STBrandEntry.class);
        } catch (Exception e) {
            e.printStackTrace();
            brandEntry = new STBrandEntry(context);
        } finally {
            return brandEntry;
        }
    }

    public String toJson()
    {
        return new Gson().toJson(this);
    }

    // region getters/setters

    public Long getToneID()
    {
        return this.toneID;
    }

    public String getClientID()
    {
        return this.clientID;
    }

    public Integer getAdID()
    {
        return this.adID;
    }

    public Integer getChannelID()
    {
        return this.channelID;
    }

    public Integer getLikeCount()
    {
        return this.likeCount;
    }

    public String getBrandName()
    {
        return this.brandName;
    }

    public String getMediaName()
    {
        return this.mediaName;
    }

    public String getUrlLogoImage()
    {
        return this.urlLogoImage;
    }

    public String getUrlLandingImage()
    {
        return this.urlLandingImage;
    }

    public String getPromoTitle()
    {
        return this.promoTitle;
    }

    public String getPromoText()
    {
        return this.promoText;
    }

    public String getLandingPageZipURL()
    {
        return this.landingPageZipURL;
    }

    public Map<String, String> getButtonNames()
    {
        return this.buttonNames;
    }

    public Map<String, String> getButtonURLs()
    {
        return this.buttonURLs;
    }

    public Map<String, String> getButtonTextColors()
    {
        return this.buttonTextColors;
    }

    public Map<String, String> getButtonBackgroundColors()
    {
        return this.buttonBackgroundColors;
    }

    public ArrayList<ButtonData> getButtonDataList()
    {
        return this.buttonDataList;
    }

    public Boolean getIsDeleted()
    {
        return this.isDeleted;
    }

    public Boolean getIsFavorited()
    {
        return this.isFavorited;
    }

    public Boolean getIsNew()
    {
        return this.isNew;
    }

    // endregion

    public void setIsDeleted(Boolean isDeleted, boolean sendValueChangedBroadcast)
    {
        if (this.isDeleted != isDeleted) {
            this.isDeleted = isDeleted;
            if (this.isDeleted) {
                this.isFavorited = false;
                this.isNew = false;
            }
            if (sendValueChangedBroadcast) {
                this.sendValueChangedBroadcast(true, false, false);
            }
        }
    }

    public boolean toggleIsDeleted(boolean sendValueChangedBroadcast)
    {
        this.setIsDeleted(!this.isDeleted, sendValueChangedBroadcast);
        return this.isDeleted;
    }

    public void setIsFavorited(boolean isFavorited, boolean sendValueChangedBroadcast)
    {
        if (this.isFavorited != isFavorited) {
            this.isFavorited = isFavorited;
            if (sendValueChangedBroadcast) {
                this.sendValueChangedBroadcast(false, true, false);
            }
        }
    }

    public boolean toggleIsFavorited(boolean sendValueChangedBroadcast)
    {
        this.setIsFavorited(!this.isFavorited, sendValueChangedBroadcast);
        return this.isFavorited;
    }

    public void setIsNew(Boolean isNew, boolean sendValueChangedBroadcast)
    {
        if (this.isNew != isNew) {
            this.isNew = isNew;
            if (sendValueChangedBroadcast) {
                this.sendValueChangedBroadcast(false, false, true);
            }
        }
    }

    private void sendValueChangedBroadcast(boolean isDeletedValueChanged, boolean isFavoritedValueChanged, boolean isNewValueChanged)
    {
        Intent intent = new Intent();
        intent.setAction(STBrandEntry.onBrandEntryValueChangedIntentFilter.getAction(0));
        intent.putExtra("toneID", this.toneID);
        intent.putExtra("isDeletedValueChanged", isDeletedValueChanged);
        intent.putExtra("isFavoritedValueChanged", isFavoritedValueChanged);
        intent.putExtra("isNewValueChanged", isNewValueChanged);

        if (this.context != null) {
            this.context.sendBroadcast(intent);
        }
    }

    public String toString()
    {
        return this.toJson();
    }

    @Override public int compareTo(STBrandEntry brandEntry)
    {
        return this.brandName.compareTo(brandEntry.brandName);
    }

    private static class Deserializer implements JsonDeserializer<STBrandEntry>
    {
        private Context context;

        public Deserializer(Context context)
        {
            this.context = context;
        }

        @Override public STBrandEntry deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
        {
            JsonObject jsonObject = json.getAsJsonObject();
            System.out.println(jsonObject.toString());

            final STBrandEntry brandEntry = new STBrandEntry(this.context);

            if (jsonObject.has("Tone_ID") && !jsonObject.get("Tone_ID").isJsonNull()) {
                brandEntry.toneID = jsonObject.get("Tone_ID").getAsLong();
            }
            if (jsonObject.has("clientID") && !jsonObject.get("clientID").isJsonNull()) {
                brandEntry.clientID = jsonObject.get("clientID").getAsString();
            }
            if (jsonObject.has("Ad_ID") && !jsonObject.get("Ad_ID").isJsonNull()) {
                brandEntry.adID = jsonObject.get("Ad_ID").getAsInt();
            }
            if (jsonObject.has("Channel_ID") && !jsonObject.get("Channel_ID").isJsonNull()) {
                brandEntry.channelID = jsonObject.get("Channel_ID").getAsInt();
            }
            if (jsonObject.has("Like_Count") && !jsonObject.get("Like_Count").isJsonNull()) {
                brandEntry.likeCount = jsonObject.get("Like_Count").getAsInt();
            }
            if (jsonObject.has("Brand_Name") && !jsonObject.get("Brand_Name").isJsonNull()) {
                brandEntry.brandName = jsonObject.get("Brand_Name").getAsString();
            }
            if (jsonObject.has("Media_Name") && !jsonObject.get("Media_Name").isJsonNull()) {
                brandEntry.mediaName = jsonObject.get("Media_Name").getAsString();
            }
            if (jsonObject.has("URL_LogoImage") && !jsonObject.get("URL_LogoImage").isJsonNull()) {
                brandEntry.urlLogoImage = jsonObject.get("URL_LogoImage").getAsString();
            }
            if (jsonObject.has("URL_LandingImage") && !jsonObject.get("URL_LandingImage").isJsonNull()) {
                brandEntry.urlLandingImage = jsonObject.get("URL_LandingImage").getAsString();
            }
            if (jsonObject.has("Promo_Title") && !jsonObject.get("Promo_Title").isJsonNull()) {
                brandEntry.promoTitle = jsonObject.get("Promo_Title").getAsString();
            }
            if (jsonObject.has("Promo_Text") && !jsonObject.get("Promo_Text").isJsonNull()) {
                brandEntry.promoText = jsonObject.get("Promo_Text").getAsString();
            }
            if (jsonObject.has("landingPageZip") && !jsonObject.get("landingPageZip").isJsonNull()) {
                brandEntry.landingPageZipURL = jsonObject.get("landingPageZip").getAsString();
            }

            // Get button data
            ArrayList<String> buttonNamesList = new ArrayList<String>();
            ArrayList<String> buttonURLsList = new ArrayList<String>();
            ArrayList<String> buttonTextColorsList = new ArrayList<String>();
            ArrayList<String> buttonBackgroundColorsList = new ArrayList<String>();
            ArrayList<String> actionTypeList = new ArrayList<String>();
            ArrayList<String> eventTitleList = new ArrayList<String>();
            ArrayList<String> eventNotesList = new ArrayList<String>();
            ArrayList<String> locationNameList = new ArrayList<String>();
            ArrayList<String> locationLatList = new ArrayList<String>();
            ArrayList<String> locationLongList = new ArrayList<String>();
            ArrayList<String> dateTimeList = new ArrayList<String>();
            ArrayList<String> emailToAddressList = new ArrayList<String>();
            ArrayList<String> emailSubjectList = new ArrayList<String>();
            ArrayList<String> emailBodyList = new ArrayList<String>();
            ArrayList<String> phoneNumberList = new ArrayList<String>();
            ArrayList<String> fbURLList = new ArrayList<String>();
            ArrayList<String> textMessageBodyList = new ArrayList<String>();

            if (jsonObject.has("Button_Text") && !jsonObject.get("Button_Text").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("Button_Text").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonNames = pair.first;
                buttonNamesList = pair.second;
            }
            if (jsonObject.has("URL_ClickThru") && !jsonObject.get("URL_ClickThru").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("URL_ClickThru").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonURLs = pair.first;
                buttonURLsList = pair.second;
            }
            if (jsonObject.has("Button_Color_FG") && !jsonObject.get("Button_Color_FG").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("Button_Color_FG").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonTextColors = pair.first;
                buttonTextColorsList = pair.second;
            }
            if (jsonObject.has("Button_Color_BG") && !jsonObject.get("Button_Color_BG").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("Button_Color_BG").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonBackgroundColors = pair.first;
                buttonBackgroundColorsList = pair.second;
            }
            if (jsonObject.has("actionType") && !jsonObject.get("actionType").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("actionType").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonActionTypes = pair.first;
                actionTypeList = pair.second;
            }
            if (jsonObject.has("eventTitle") && !jsonObject.get("eventTitle").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("eventTitle").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonEventTitles = pair.first;
                eventTitleList = pair.second;
            }
            if (jsonObject.has("eventNotes") && !jsonObject.get("eventNotes").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("eventNotes").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonEventNotes = pair.first;
                eventNotesList = pair.second;
            }
            if (jsonObject.has("locationName") && !jsonObject.get("locationName").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("locationName").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonLocationNames = pair.first;
                locationNameList = pair.second;
            }
            if (jsonObject.has("locationLat") && !jsonObject.get("locationLat").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("locationLat").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonLocationLats = pair.first;
                locationLatList = pair.second;
            }
            if (jsonObject.has("locationLong") && !jsonObject.get("locationLong").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("locationLong").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonLocationLongs = pair.first;
                locationLongList = pair.second;
            }
            if (jsonObject.has("dateTime") && !jsonObject.get("dateTime").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("dateTime").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonDateTimes = pair.first;
                dateTimeList = pair.second;
            }
            if (jsonObject.has("emailToAddress") && !jsonObject.get("emailToAddress").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("emailToAddress").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonEmailToAddresses = pair.first;
                emailToAddressList = pair.second;
            }
            if (jsonObject.has("emailSubject") && !jsonObject.get("emailSubject").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("emailSubject").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonEmailSubjects = pair.first;
                emailSubjectList = pair.second;
            }
            if (jsonObject.has("emailBody") && !jsonObject.get("emailBody").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("emailBody").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonEmailBodys = pair.first;
                emailBodyList = pair.second;
            }
            if (jsonObject.has("phoneNumber") && !jsonObject.get("phoneNumber").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("phoneNumber").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonPhoneNumbers = pair.first;
                phoneNumberList = pair.second;
            }
            if (jsonObject.has("fbURL") && !jsonObject.get("fbURL").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("fbURL").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonFBURLs = pair.first;
                fbURLList = pair.second;
            }
            if (jsonObject.has("textMessageBody") && !jsonObject.get("textMessageBody").isJsonNull()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.get("textMessageBody").getAsJsonObject().entrySet();
                Pair<TreeMap<String, String>, ArrayList<String>> pair = parseEntrySet(entrySet);
                brandEntry.buttonTextMessageBodys = pair.first;
                textMessageBodyList = pair.second;
            }

			for (int i = 0; i < buttonNamesList.size(); i += 1) {

				ButtonData buttonData = new ButtonData();

				buttonData.text = buttonNamesList.get(i);
				try { buttonData.url = buttonURLsList.get(i); } catch (Exception e) { }
				try { buttonData.colorFG = buttonTextColorsList.get(i); } catch (Exception e) { }
				try { buttonData.colorBG = buttonBackgroundColorsList.get(i); } catch (Exception e) { }

				if (buttonData.validate()) {
					brandEntry.buttonDataList.add(buttonData);
				} else {
					System.out.println("Invalid Brand Entry Button Data!");
				}
			}

            if (jsonObject.has("isDeleted")) { brandEntry.isDeleted = jsonObject.get("isDeleted").getAsBoolean(); }
            if (jsonObject.has("isFavorited")) { brandEntry.isFavorited = jsonObject.get("isFavorited").getAsBoolean(); }
            if (jsonObject.has("isNew")) { brandEntry.isNew = jsonObject.get("isNew").getAsBoolean(); }

            return brandEntry;
        }
    }

    private static Pair<TreeMap<String, String>, ArrayList<String>> parseEntrySet(Set<Map.Entry<String, JsonElement>> set)
    {
        TreeMap<String, String> treeMap = new TreeMap<String, String>();
        ArrayList<String> arrayList = new ArrayList<String>();

        Iterator<Map.Entry<String, JsonElement>> itr = set.iterator();
        while (itr.hasNext()) {
            Map.Entry<String, JsonElement> entry = itr.next();
            String value = entry.getValue().getAsString();
            if (value.length() > 0) {
                treeMap.put(entry.getKey(), value);
                arrayList.add(value);
            }
        }

        return new Pair<>(treeMap, arrayList);
    }





    public static class ButtonData
    {
        private String text = null;
        private String url = "";
        private String colorFG = "";
        private String colorBG = "";

        public enum ActionType
        {
            NONE(-1),
            STANDARD(0),    // standard link (no additional data required)
            CALENDAR(1),    // calendar (uses eventTitle, locationName, locationLat, locationLong, dateTime, eventNotes)
            EMAIL(2),       // email (uses emailToAddress, emailSubject, emailBody)
            MAPS(3),        // maps (uses locationName, locationLat, locationLong)
            PHONE(4),       // phone (uses phoneNumber)
            SMS(5),         // textMessage (uses phoneNumber, textMessageBody)
            FACEBOOK(6),    // facebookDeepLink (uses fbURL)
            TWITTER(7),     // twitter post (uses textMessageBody)
            REMINDER(8);    // reminder (uses eventTitle, dateTime, eventNotes)

            private int value;
            public int getValue() { return this.value; }

            ActionType(int value)
            {
                this.value = value;
            }
        }

        private ActionType actionType = ActionType.NONE;
        private String eventTitle = "";
        private String eventNotes = "";
        private String locationName = "";
        private String locationLat = "";
        private String locationLong = "";
        private String dateTime = "";
        private String emailToAddress = "";
        private String emailSubject = "";
        private String emailBody = "";
        private String phoneNumber = "";
        private String fbURL = "";
        private String textMessageBody = "";

        // region getters/setters

        public String getText()
        {
            return this.text;
        }

        public String getUrl()
        {
            return this.url;
        }

        public String getColorFG()
        {
            return this.colorFG;
        }

        public String getColorBG()
        {
            return this.colorBG;
        }

        public ActionType getActionType()
        {
            return actionType;
        }

        public String getEventTitle()
        {
            return eventTitle;
        }

        public String getEventNotes()
        {
            return eventNotes;
        }

        public String getLocationName()
        {
            return locationName;
        }

        public String getLocationLat()
        {
            return locationLat;
        }

        public String getLocationLong()
        {
            return locationLong;
        }

        public String getDateTime()
        {
            return dateTime;
        }

        public String getEmailToAddress()
        {
            return emailToAddress;
        }

        public String getEmailSubject()
        {
            return emailSubject;
        }

        public String getEmailBody()
        {
            return emailBody;
        }

        public String getPhoneNumber()
        {
            return phoneNumber;
        }

        public String getFbURL()
        {
            return fbURL;
        }

        public String getTextMessageBody()
        {
            return textMessageBody;
        }

        // endregion

        public boolean validate()
        {
			return !TextUtils.isEmpty(this.text) && !TextUtils.isEmpty(this.url);
//            return !TextUtils.isEmpty(this.text) &&
//                    !TextUtils.isEmpty(this.url) &&
//                    !TextUtils.isEmpty(this.colorFG) &&
//                    !TextUtils.isEmpty(this.colorBG);
        }
    }
}




