package fiveelementsentertainment.smarttones;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by patsluth on 16-09-23.
 */
public abstract class STPermissionsHelper
{
    public static final int ST_PERMISSIONS_REQUEST_RECORD_AUDIO = 1000;
    public static final int ST_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1001;

    public static boolean hasRecordAudioPermission(Context context)
    {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasLocationPermission(Context context)
    {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean confirmPermissions(Activity activity)
    {
        if (!STPermissionsHelper.hasRecordAudioPermission(activity) || !STPermissionsHelper.hasLocationPermission(activity)) {
            String temp[] = new String[2];
            temp[0] = Manifest.permission.RECORD_AUDIO;
            temp[1] = Manifest.permission.ACCESS_COARSE_LOCATION;
            ActivityCompat.requestPermissions(activity, temp, ST_PERMISSIONS_REQUEST_RECORD_AUDIO & ST_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
            return false;
        }

        return true;
    }
}




