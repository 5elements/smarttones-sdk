package fiveelementsentertainment.smarttones;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by patsluth on 2016-10-11.
 */

public final class STAnalytics
{
    // http://smarttones.shalamov.net/logAnalytics?authToken=37A8AB91-DE3A-4C31-B9AD-75A60C1A&eventType=Tone_Detected&p1Key=UserID&p1Val=1234&p2Key=ToneID&p2Val=5678
    private static String ANALYTICS_AUTH_TOKEN = "37A8AB91-DE3A-4C31-B9AD-75A60C1A";

    public static String ANALYTICS_EVENT_TYPE_TONE_DELETED = "Tone_Deleted";
    public static String ANALYTICS_EVENT_TYPE_TONE_FAVORITED = "Tone_Favorited";
    public static String ANALYTICS_EVENT_TYPE_TONE_UNFAVORITED = "Tone_Unfavorited";

    /**
     * Log Event. eventParams may have up 10 values
     * @param eventType
     * @param eventParams
     */
    public static void logEvent(Context context, String eventType, Map<String, String> eventParams)
    {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            StringBuilder urlBuilder = new StringBuilder(STToneManager.ST_API_URL)
                    .append("logAnalytics?")
                    .append("authToken=")
                    .append(ANALYTICS_AUTH_TOKEN)
                    .append("&eventType=")
                    .append(eventType)
                    .append("&deviceType=android");

            Iterator<String> itr = eventParams.keySet().iterator();
            int i = 0;
            while (itr.hasNext() && i < 10) {
                i += 1;
                String key = itr.next();

                // ex - "&p1Key=UserID&p1Val=1234&p2Key=ToneID&p2Val=5678"
                urlBuilder
                        .append("&p")
                        .append(i)
                        .append("Key=")
                        .append(key)
                        .append("&p")
                        .append(i)
                        .append("Val=")
                        .append(eventParams.get(key));
            }

            System.out.println("logEvent: " + urlBuilder.toString());

            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    urlBuilder.toString(),
                    new Response.Listener<String>()
                    {
                        @Override public void onResponse(String response)
                        {
                            System.out.println(response);
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override  public void onErrorResponse(VolleyError error)
                        {
                            System.out.println(error.getLocalizedMessage());
                        }
                    });

            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Map<String, String> getEventParamsForBrandEntry(STBrandEntry brandEntry)
    {
        Map<String, String> eventParams = new TreeMap<>();

        eventParams.put("ToneID", (brandEntry != null) ? brandEntry.getToneID().toString() : "-1");
        eventParams.put("UserServiceType", STUserManager.i().getUser().loginType.getServiceName());
        eventParams.put("UserID", STUserManager.i().getUser().userID);

        return eventParams;
    }
}




